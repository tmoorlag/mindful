module.exports = {
  root: true,
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
    'no-debugger': false
  },
  extends: [
    'plugin:vue/essential',
    'plugin:prettier/recommended',
    'eslint:recommended'
  ]
}
