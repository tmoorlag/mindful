/* eslint-disable no-debugger */
const localStorageCacheKey = 'mindful_data_cache'

function setLocalStorage(key, data) {
  localStorage.setItem(key, JSON.stringify(data))
}

function getLocalStorage(stateKey, defaultValue) {
  let storageState = localStorage.getItem(localStorageCacheKey)

  if (!storageState) {
    return defaultValue
  }

  storageState = JSON.parse(storageState)

  return storageState[stateKey]
}

export const namespaced = true
export const state = {
  lists: getLocalStorage('lists', []),
  listItems: getLocalStorage('listItems', {}),
  collections: getLocalStorage('collections', []),
  selectedCollectionId: getLocalStorage('selectedCollectionId', false)
}

export const getters = {
  getListItems(state) {
    return (listId) => {
      if (!state.listItems[listId]) {
        return []
      }

      return state.listItems[listId]
    }
  },
  canShowCollectionBar(state) {
    const collectionLength = state.collections.length

    if (collectionLength == 0 && state.lists.length >= 1) {
      return true
    }

    return collectionLength >= 1
  },
  selectedCollection(state) {
    return state.collections.find(
      (collection) => collection.id === state.selectedCollectionId
    )
  },
  selectedCollectionLists(state, getters) {
    if (state.collections.length <= 0 && !state.selectedCollectionId) {
      return state.lists
    }

    const selectedCollection = getters.selectedCollection
    if (selectedCollection.listIds.length <= 0 && state.collections >= 2) {
      return state.lists
    }

    const filteredLists = state.lists.filter((list) => {
      return selectedCollection.listIds.includes(list.id)
    })

    return filteredLists
  }
}

export const mutations = {
  ADD_LIST(state, list) {
    state.lists.push(list)
  },
  DELETE_LIST(state, list) {
    state.lists.splice(state.lists.indexOf(list), 1)
    const listIndex = state.lists.findIndex((_list) => _list.id === list.id)

    state.lists = state.lists.splice(listIndex)
  },
  ADD_COLLECTION(state, collection) {
    state.collections.push(collection)
  },
  ADD_LIST_TO_COLLECTION(state, { collectionId, listId }) {
    const collections = state.collections.map((c) => {
      if (c.id === collectionId) {
        c.listIds.push(listId)
      }

      return c
    })

    state.collections = collections
  },
  ADD_ITEM_TO_LIST(state, { listId, item }) {
    if (!state.listItems[listId]) {
      state.listItems[listId] = []
    }

    state.listItems[listId].push(item)
  },
  UPDATE_LIST_ITEM(state, { listId, item }) {
    const items = state.listItems[listId]

    state.listItems[listId] = Object.assign(items, item)
  },
  SET_SELECTED_COLLECTION_ID(state, collectionId) {
    state.selectedCollectionId = collectionId
  }
}

export const actions = {
  setSelectedCollectionId(context, collectionId) {
    context.commit('SET_SELECTED_COLLECTION_ID', collectionId)

    context.dispatch('updateLocalStorage')
  },
  updateLocalStorage(context) {
    setLocalStorage(localStorageCacheKey, context.state)

    console.log('syncing with localstorage', context.state)
  },
  createList(context, list) {
    const listId = list.id
    const selectedCollectionId = context.state.selectedCollectionId

    if (selectedCollectionId) {
      context.commit('ADD_LIST_TO_COLLECTION', {
        collectionId: selectedCollectionId,
        listId
      })
    }

    context.commit('ADD_LIST', list)
    context.dispatch('updateLocalStorage')
  },
  createListItem(context, payload) {
    context.commit('ADD_ITEM_TO_LIST', payload)

    context.dispatch('updateLocalStorage')
  },
  updateListItem(context, payload) {
    context.commit('UPDATE_LIST_ITEM', payload)

    context.dispatch('updateLocalStorage')
  },
  deleteList(context, list) {
    context.commit('DELETE_LIST', list)
    context.dispatch('updateLocalStorage')
  },
  createCollection(context, collection) {
    collection.listIds = []
    context.commit('ADD_COLLECTION', collection)

    if (!context.state.selectedCollectionId) {
      context.commit('SET_SELECTED_COLLECTION_ID', collection.id)
      context.state.lists.forEach((list) => {
        context.commit('ADD_LIST_TO_COLLECTION', {
          listId: list.id,
          collectionId: collection.id
        })
      })
    }

    context.dispatch('updateLocalStorage')
  }
}
