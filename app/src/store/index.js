import { createStore, createLogger } from 'vuex'
import * as app from './modules/app'

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    app
  },
  plugins: [createLogger()]
})
