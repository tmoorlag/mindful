export default class FormStateEngine {
  createInitialState(source, stateTree) {}
  stateHasChanges(source, state) {}
  getStateChanges(source, state) {}
}
