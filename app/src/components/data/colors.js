const colors = [
  {
    name: 'blue',
    class: 'bg-blue-400'
  },
  {
    name: 'green',
    class: 'bg-green-400'
  },
  {
    name: 'yellow',
    class: 'bg-yellow-400'
  },
  {
    name: 'purple',
    class: 'bg-purple-400'
  },
  {
    name: 'red',
    class: 'bg-red-400'
  }
]

export { colors }
